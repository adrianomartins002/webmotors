import "./style.css";
import { FilterProvider, useFilter } from "../../context/filter";
import { Header } from "../../components/Header";
import { BoxFilter } from "../../components/BoxFilter";
import { useEffect, useState } from "react";
import { Services } from "../../services/services";
import { VehicleCard } from "../../components/Filters/VehicleCard";



export function Principal() {
    const [vehicles, setVehicles] = useState([]);
    const [currentPage, setCurrentPage] = useState(1);
    const {filter} = useFilter();
    

    async function searchVehiclesByFilter(){
        let result = await Services.getVehicles(currentPage, filter);

        if(result.data){
            
            setVehicles([...vehicles, ...result.data]);
        }
    }
    
    const handleScroll = () => {
        let userScrollHeight = window.innerHeight + window.scrollY;
        let windowBottomHeight = document.documentElement.offsetHeight;
        
        if (userScrollHeight >= (windowBottomHeight+1000)) {
            console.log("entrou");
            console.log("current page:", currentPage);

            setCurrentPage(currentPage+1);
            searchVehiclesByFilter();
        }   
  };


  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
    // eslint-disable-next-line
  }, []);   


    return (
        <FilterProvider>
            <div className="principal-container">
                <Header />
                <BoxFilter searchResults={searchVehiclesByFilter}/>
            </div>
            <div className="vehicles-container">
                {vehicles.map( item => <VehicleCard key={item.ID} vehicleDetails={item} />)}
            </div>
        </FilterProvider>
    );
}