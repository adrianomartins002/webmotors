import {fireEvent, getByRole, render} from '@testing-library/react'
import { NewComboBox } from '../components/NewComboBox'; 

describe('should create the list', ()=>{
    test('if pass the label, show the label',() => {
        const { getByText } = render(<NewComboBox label="Marcas:"/>)
        expect( getByText(/Marcas:/i)).toBeInTheDocument();
       }),
     test('if pass the itemSelect prop, should exec the function',()=>{
        const onItemSelect = jest.fn();
        const { container } = render(<NewComboBox label="Marcas:" selectItem={onItemSelect}/>)
        fireEvent.click(container.querySelector('.select-box--items div'));
        expect(onItemSelect).toHaveBeenCalled();
     })
    }
 )


