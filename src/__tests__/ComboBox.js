import {fireEvent, getByRole, render} from '@testing-library/react'
import { ComboBox } from '../components/ComboBox'; 

describe('should create the list', ()=>{
    test('if pass the label, show the label',() => {
        const { getByText } = render(<ComboBox label="Marcas:" listOfItems={[{Name:"test", ID:1}]}/>)
        expect( getByText(/Marcas:/i)).toBeInTheDocument();
       })
     ,test('if pass the itemSelect prop, should exec the function',()=>{
        const onItemSelect = jest.fn();
        const { container } = render(<ComboBox label="Marcas:" listOfItems={[{Name:"test", ID:1}]} changeItem={onItemSelect}/>)
        fireEvent.change(container.querySelector('.combobox-selection'));
        expect(onItemSelect).toHaveBeenCalled();
     })
    }
 )


