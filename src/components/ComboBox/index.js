import "./style.css";
import React  from 'react';

export const ComboBox = React.memo(({
    name,
    label,
    listOfItems,
    styleContainer,
    styleSelect,
    styleLabel,
    changeItem,
    disabled,
    className="combobox-container",
})=>{
  
    async function handleChangeMake(event){
        const index = event.target.selectedIndex;
        const el = event.target.childNodes[index]
        const optionSelected =  el.getAttribute('id');  
        
        changeItem(
             optionSelected.split("-")[1],
            event.target.name
        )

    }

    

    return(
        <div className={className} style={{...styleContainer}}>
        <label style={{...styleLabel}}>{label}</label>
        <select style={{...styleSelect}} disabled={disabled} className="combobox-selection" name={name} onChange={(event)=>handleChangeMake(event)}>
            {listOfItems.map( item => <option key={`${item.Name}-${item.ID}`} id={`${item.Name}-${item.ID}`} value={item.Name}> {item.Name} </option>)}
        </select>
        </div>
    );

})