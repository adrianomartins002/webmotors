import { useState } from 'react';
import { FaCaretDown, FaCaretUp } from "react-icons/fa";
import "./style.css";

export function NewComboBox({
    label,
    listOfItems = [{ value: "Todos", id: 1 }],
    selectItem,
}) {
    const [items, ] = useState(listOfItems);
    const [showItems, setShowItems] = useState(false);
    const [selectedItem, setSelectedItem] = useState(listOfItems[0]);


    const dropDown = () => {
        if (showItems) {
            setShowItems(false);
        } else {
            setShowItems(true);
        }
    }

    
    const selectNewItem = item => {
        setSelectedItem(item);
        setShowItems(false);
        selectItem(item);
    }


    return (
        <div className="combobox" onClick={dropDown}>
            <div className="combobox--container">
                {label ?
                    <label className='combobox-label'>
                        {label}
                    </label>
                    :
                    null
                }

                <div className="combobox--selected-item">
                    {selectedItem.value}
                </div>
                <div className="combobox--arrow">

                        {showItems ?
                            <FaCaretUp />
                            :
                            <FaCaretDown />

                        }
                </div>

                <div
                    style={{ display: showItems ? "block" : "none" }}
                    className={"select-box--items"}
                >
                    {items.map(item => (
                        <div
                            key={item.id}
                            onClick={() => selectNewItem(item)}
                            className={selectedItem === item ? "selected" : ""}
                        >
                            {item.value}
                        </div>
                    ))}
                </div>
            </div>
        </div>
    )
}