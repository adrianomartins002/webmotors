import "./style.css"


export function Header() {
    return (
        <div className="box-header">
            <div className="cars-area-active">
                <label>
                    Comprar
                </label>
                <span>
                    Carros
                </span>
            </div>
            <div className="cars-area">
                <label>
                    Comprar
                </label>
                <span>
                    Motos
                </span>
            </div>
        </div>
    )
}