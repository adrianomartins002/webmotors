import { useState, useEffect} from "react";
import { useFilter } from "../../../context/filter";
import { Services } from "../../../services/services";
import { ComboBox } from "../../ComboBox";

export function VersionBox() {
    const { filter, setFilter } = useFilter();
    const [versions, setVersions] = useState();
    
    async function searchVersions(){
       
        if(filter && filter.model){
            let versionsData = await Services.getVersion(filter.model.id)
            setVersions(versionsData.data)
        }
    }

    async function changeVersionFilter(id, name){
        setFilter({
            ...filter,
            version:{
                id,
                name
            }
        })
    }
    
    useEffect(()=>{
        
        searchVersions()
        // eslint-disable-next-line
    },[filter])

    if (versions && versions.length > 0) {
        return (
            <ComboBox tyleSelect={{
                width: "85%"
            }} className= "combobox-container-version" label="Versao:" listOfItems={[{Name: "Todos as versões"}, ...versions]} changeItem={changeVersionFilter}/>
        )
    }

    return (
    <ComboBox styleSelect={{
        width: "85%"
    }} className="combobox-container-version" label="Versao:" listOfItems={[{ID: Math.random(),Name: "Todos"}]} />
    );
}