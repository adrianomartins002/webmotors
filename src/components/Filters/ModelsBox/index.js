import { useState, useEffect, useCallback} from "react";
import { useFilter } from "../../../context/filter";
import { Services } from "../../../services/services";
import { ComboBox } from "../../ComboBox";

export function ModelsBox() {
    const { filter, setFilter } = useFilter();
    const [models, setModels] = useState();
    
    const searchModels = useCallback(async ()=>{
        if(filter && filter.make){
            let modelsData = await Services.getModel(filter.make.id)
            setModels(modelsData.data)
        }
    }, [filter])

    async function changeModelFilter(id, name){
        setFilter({
            ...filter,
            model:{
                id,
                name
            }
        })
    }
    
    useEffect(()=>{
       
        searchModels()
    },[filter, searchModels])

    if (models && models.length > 0) {
        return (
            <ComboBox disabled={false} label="Modelo:" listOfItems={[{Name: "Todos os models"}, ...models]} changeItem={changeModelFilter}/>
        )
    }

    return (
    <ComboBox disabled={true} label="Modelo:" listOfItems={[{ID: Math.random(),Name: "Todos"}]} />
    );
}