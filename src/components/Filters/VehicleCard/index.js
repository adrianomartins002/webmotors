import "./style.css";

export function VehicleCard({
    vehicleDetails
}){
    
    return(
        <div className="container-vehicle-card">
            <img src={vehicleDetails.Image} alt={vehicleDetails.Model} width={200} height={200}/>
            <div className="container-vehicle-details">
                <span className="make-vehicle">{vehicleDetails.Make}</span> 
                <span className="model-vehicle">{vehicleDetails.Model}</span>
                <span className="price-vehicle">{vehicleDetails.Price}</span>
            </div>
        </div>    
    );
}