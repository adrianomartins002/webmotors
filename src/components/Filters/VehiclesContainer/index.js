import { VehicleCard } from "../VehicleCard";

export function VehiclesContainer({vehicles}){
    console.log("vehicles:", vehicles)
    return(
        <div>
            {vehicles.map(item=><VehicleCard vehicleDetails={item}/>)}
        </div>

    );
}