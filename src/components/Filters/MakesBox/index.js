import { Services } from "../../../services/services";
import {useFilter} from "../../../context/filter";
import { useEffect, useState } from "react";
import { ComboBox } from "../../ComboBox";

export function MakesBox(){
    const [makes, setMakes] = useState();
    const {filter, setFilter} = useFilter();

    async function searchMakes() {
        let makes = await Services.getMake();
        setMakes(makes.data);
       
    }

    useEffect(() => {
        searchMakes();
    }, [])

    function changeMake(id, name){
        setFilter({
            ...filter,
            make: {
                id,
                name
            }
        });
    }

    if(makes && makes.length > 0){
        return(
            <ComboBox label="Marca:" name="marcas" listOfItems={makes} changeItem={changeMake}/>
        )
    }

    return(
        <ComboBox label="Marca:" name="marcas" listOfItems={[{id: Math.random(), Name: "Todas"}]} />
    )
    
}