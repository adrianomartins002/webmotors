import { useState} from "react";
import { useFilter } from "../../../context/filter";
import { ComboBox } from "../../ComboBox";

export function YearBox() {
    const { filter, setFilter } = useFilter();
    const [versions,] = useState([{Name: "2010", ID: 2010}, {Name: "2011", ID: 2011}, {Name: "2012", ID: 2021}]);
    
    async function changeModelFilter(id, name){
        setFilter({
            ...filter,
            year:{
                id,
                name
            }
        })
    }
    

    if (versions && versions.length > 0) {
        return (
            <ComboBox label="Ano desejado" listOfItems={versions} changeItem={changeModelFilter}/>
        )
    }

    return (
        <ComboBox label="Ano desejado" listOfItems={[{ID: Math.random(),Name: ""}]} />
    );
}