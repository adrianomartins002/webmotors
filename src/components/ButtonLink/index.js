
import "./style.css";

export function ButtonLink({
    onClick,
    description,
    style
}){

    return(
        <button className="buttom-link" style={{...style}} onClick={onClick}> 
            {description}
        </button>
    )

}