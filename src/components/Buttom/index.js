
import "./style.css";

export function Button({
    isPrimary,
    onClick,
    description,
    style
}){

    return(
        <button onClick={onClick} className={`buttom-${isPrimary? "primary" : "secondary"}`} style={{...style}}>
            {description}
        </button>
    )

}