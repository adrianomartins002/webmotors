import "./style.css";

export function Checkbox({
    label,
}) {

    return (
        <div className="checkbox-container">
            <input type="checkbox" className="checkbox"/>
            <label>{label}</label>
        </div>
    )

}
