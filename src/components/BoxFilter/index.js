import { Button } from "../Buttom";
import { ButtonLink } from "../ButtonLink";
import { Checkbox } from "../CheckBox";
import { ComboBox } from "../ComboBox";
import { ComboBoxLocation } from "../ComboBoxLocation";
import "./style.css"

import { MakesBox } from "../Filters/MakesBox";
import { ModelsBox } from "../Filters/ModelsBox";
import { YearBox } from "../Filters/YearBox";
import { VersionBox } from "../Filters/VersionBox";

import { useFilter } from "../../context/filter";



export function BoxFilter({searchResults}) {
   
    const { setFilter } = useFilter();


    return (
        <div className="box-container">
            <div className="box-container-header">
                <Checkbox label="Novos" />
                <Checkbox label="Usados" />

            </div>
            <div className="box-body">
               
                <ComboBoxLocation label="Onde:" name="location" listOfRange={[{ Name: "1 km" }, { Name: "10 km" }, { Name: "50 km" }, { Name: "100 km" },]} listOfLocations={[{ description: "São Paulo" }, { description: "Rio de Janeiro" }]} />
                <MakesBox />
                <ModelsBox />
                <YearBox label="Ano desejado" />
                <ComboBox styleSelect={{width: 130}} label="Faixa de preço" listOfItems={[{ Name: "Faixa de preço", ID: 1 }]} />
                <VersionBox />
            </div>
            <div className="box-footer">

               <div className="left-footer">
               <ButtonLink description={"Busca Avançada"} style={{
                    color: "#cf3e3e",
                    fontWeight: "bold",
                    paddingRight: 140,
                    cursor: "pointer"
                }} />
               </div>
                <div className="right-footer">
                    <ButtonLink description={"Limpar filtros"} onClick={() => setFilter(null)} />
                    <Button description={"ver ofertas"} isPrimary onClick={searchResults} />
                </div>
            </div>

        </div>


    )
}