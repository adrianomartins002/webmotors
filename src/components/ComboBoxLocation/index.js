import "./style.css";
import { ImLocation } from "react-icons/im";
import { AiFillCloseCircle } from "react-icons/ai";

export function ComboBoxLocation({
    name,
    label,
    listOfLocations,
    listOfRange,
    disabled = false,
}) {

    return (
        <div className="combobox-container-location-range">
            <div className="combobox-container-location">
                <ImLocation fill="#cf3e3e"/>
                <label>{label}</label>
                <select className="combobox-selection-location" name={name}>
                    {listOfLocations.map(item => <option key={item.description} value={item.description}> {item.description} </option>)}
                </select>
                <AiFillCloseCircle onClick={()=>{}}/>
            </div>
            <div className="combobox-range">
                <label >{label}</label>
                <select disabled={disabled} className="combobox-selection" name={name}>
                    {listOfRange.map(item => <option key={`${item.Name}-${item.ID}`} id={`${item.Name}-${item.ID}`} value={item.Name}> {item.Name} </option>)}
                </select>
            </div>
        </div>
    );

}