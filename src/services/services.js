import api from "./api";


export const Services = {
  getMake: async () => {
    try {
      const { data } = await api.get('/api/OnlineChallenge/Make');

      return {
        status: true,
        data,
      };
    } catch (response) {
        console.log("deixa ver;", response);
      return {
        status: false,
        error: "Não foi possível recuperar os dados",
      };
    }
  },
  getModel: async (makeId) => {
    try {
      const { data } = await api.get(`/api/OnlineChallenge/Model`, {
        params: {
          makeID: makeId
        }
      });

      return {
        status: true,
        data,
      };
    } catch (response) {
      return {
        status: false,
        error: "Não foi possível recuperar os dados",
      };
    }
  },
  getVersion: async (modelId) => {
    try {
      const { data } = await api.get(`/api/OnlineChallenge/Version`, {
        params: {
          modelID: modelId 
        }
      });

      return {
        status: true,
        data,
      };
    } catch (response) {
      return {
        status: false,
        error: "Não foi possível recuperar os dados",
      };
    }
  },
  getVehicles: async (page, filter) => {
    // teoricamente deveria utilizar o filter para criar os filtros e passar como parametro na request, mas no swagger
    // não tem explicando os parametros e como passar, somente o de paginação
    if(filter){
      console.log(filter);
      //fazer aqui a criação da string de filtro com todos os filtros selecionados
    }
    try {
      const { data } = await api.get(`/api/OnlineChallenge/Vehicles?Page=${page}`);

      return {
        status: true,
        data,
      };
    } catch (response) {
      return {
        status: false,
        error: "Não foi possível recuperar os dados",
      };
    }
  },
};