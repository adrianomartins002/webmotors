# Webmotors
Projeto para criação da solução para o site webmotos


# Iniciando
Essas instruções fornecerão uma cópia do projeto em execução na sua máquina local para fins de desenvolvimento e teste. Consulte implantação para obter notas sobre como implantar o projeto em 
um sistema ativo.

## Pré-requisitos:
Para execução e desenvolvimento do projeto é necessário, ou que tenha instalado:
- nodejs
- yarn ou npm
- Plugin para desabilitar o CORS no navegador (isso tive que utilizar para conseguir consumir da API)



## Desenvolvido com
 - JavaScript
 - [React](https://reactjs.org/)
 - [React Testing Library](https://testing-library.com/docs/react-testing-library/intro/)
 - [Axios](https://axios-http.com/docs/intro)


## Execução pro projeto
 - Para executar o projeto pelo navegador executando na própria máquina basta baixar os pacotes do package.json 
 utilizando o comando ``` yarn ou npm install ``` e logo após o download terminar, utilizar o comando ``` yarn start```.

## Executar os testes 
- Para executar os testes unitários basta rodar o comando ``` yarn test ou npm run test``` no terminal no diretório raiz.

## Deploy do projeto
- O deploy foi configurado para adicionar os arquivos ao bucket no S3 da amazon webservices, o link até o momento é ```http://webmotorstests.s3-website-us-east-1.amazonaws.com```, como não adicionei um dns padrão, pode ser que o link mude.
## CI/CD
- O CI/CD utilizei do próprio deploy do gitlab, criando um arquivo .yml e adicionando as configurações para selecionar as variaveis de ambiente configuradas no gitlab, referentes ao bucket do S3 criado na amazon webservices.
- Criei ao todo 3 estágios, o de build, o de testes e o de deploy, o de build basicamente para baixar as dependencias e verificar se o react não encontra nenhum problema no build do projeto, no stage de testes verifico se todos os testes unitários executaram de forma correta, e o de deploy pego os arquivos estáticos gerados no diretorio de build e subo para o bucket do S3 da Amazon.

## Git
- Relacionado ao git não fiz tantos pontos de melhoria, só criei os commits seguindo os padrões de feat/fix/bug etc, o ideal é que a cada commit de nova feature criasse uma nova branch para fazer o merge request, mas como só eu trabalhei no projeto achei desnecessário por enquanto.

## Swagger para objtenção dos dados:
 - [Desafio WebMotors](https://desafioonline.webmotors.com.br/swagger/ui/index#/OnlineChallenge)


## Pontos de melhoria:
- O projeto segue algumas informações disponíveis para as rotas no swagger, porém alguns pontos não foram explicados que podem fazer como melhoria:
````
    - Manipulação das combobox de acordo com a selecionada (seria possivel algumas das combobox serem desabilitadas de acordo com os dados provenientes de outra, por exemplo, se tiverem dependencias);
    - Recuperação da localização via API (não foi exibido na documentaçãou ou no swagger como buscar a cidade / região selecionada, por isso coloquei só São Paulo por default)
    - Raio e km (se encaixa no mesmo ponto acima)
    - Ver ofertas ( As ofertas foram buscadas somente com a paginação, mas creio que o correto seria aplicar os filtros recuperados (e criados no hook useFilter), porém no swagger não mostra quais outros parametros de filtros estão disponíveis) 
    - Busca avançada (Na documentação não mostra a busca avançada para inclusão dos filtros ou adição de mais itens)
    -Aba de motos (Não sei se a aba de motos faria a mesma coisa da de carros)
    - Ao recuperar os dados não exibe as imagens (não trabalhei bem na renderização dos itens ao buscar, pois não sei se era pra fazer isso, vi que nenhuma das imagens estava carregando, então imaginei que não deveria fazer essa etapa, mas mesmo assim criei a função e parte do componente de renderização.)
    - Testes: como não tive tanto tempo, a maior melhoria seria a de fazer todos os testes unitários, de renderização e execução dos componentes, coloquei alguns, somente para mostrar a padronização dos testes.
    - Criação de um componente de combobox totalmente customizado: para ganhar mais agilidade no desenvolvimento da solução, utilizei o select do proprio html, mas ele tem algumas peculiaridades, por exemplo, no macbook, não da pra customizar totalmente o dropdown, pois o macOS tem o próprio padrão do select, eu comecei a criação de um novo, mas não cheguei a utilizar.
````

## Funcionalidaes
- Utilização dos hooks do react para criação e manutenção dos componentes
- Utilização da lib ``` axios ``` para recuperação dos dados da api rest 
- Utilização da lib react-testing-library para criação de testes unitários
- Criação do .yml para build do projeto e upload para aws S3

## Autores
- Adriano Martins de Oliveira Sousa.



